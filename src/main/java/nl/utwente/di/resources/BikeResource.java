package nl.utwente.di.resources;

import nl.utwente.di.dao.BikeDao;
import nl.utwente.di.model.Bike;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;


public class BikeResource {
    @Context
    UriInfo uriInfo;
    @Context
    Request request;
    String id;

    public BikeResource(UriInfo uriInfo, Request request, String id) {
        this.uriInfo = uriInfo;
        this.request = request;
        this.id = id;
    }

    @DELETE
    public void deleteBike(@QueryParam("bikeId") String bikeId) {
        Bike c = BikeDao.instance.getModel().remove(bikeId);
        if(c==null) {
            throw new RuntimeException("Delete: Bike with " + id + " not found");
        }
    }

    @PUT
    public void updateBike(@QueryParam("bikeId") String bikeId, @QueryParam("ownerName") String ownerName, @QueryParam("colour") String colour, @QueryParam("gender") String gender) {
        BikeDao.instance.getModel().put(bikeId, new Bike(bikeId, ownerName, colour, gender));
        if(BikeDao.instance.getModel().get(bikeId)==null) {
            throw new RuntimeException("Update: Bike with " + id + " not found");
        }
        if(bikeId.equals(null) || ownerName.equals(null) || colour.equals(null) || gender.equals(null)) {
            throw new RuntimeException("Update: Bike with " + id + " not possible, invalid input");
        }
    }
}
