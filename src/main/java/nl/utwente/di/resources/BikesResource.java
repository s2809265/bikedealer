package nl.utwente.di.resources;

import nl.utwente.di.dao.BikeDao;
import nl.utwente.di.model.Bike;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.JAXBElement;

// Will map the resource to the URL todos
@Path("/bikes")
public class BikesResource {

    // Allows to insert contextual objects into the class,
    // e.g. ServletContext, Request, Response, UriInfo
    @Context
    UriInfo uriInfo;
    @Context
    Request request;

    Map<String, Bike> bikes = BikeDao.instance.getModel();
    Map<String, Boolean> orderedBikes = new HashMap<>();

    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response createBikeAndGetResponse(JAXBElement<Bike> bike) {
        Response response;
        if(!bikes.containsKey(bike)) {
            bikes.put(bike.getValue().getId(), bike.getValue());
            orderedBikes.put(bike.getValue().getId(), false);
            response = Response.ok(bike).build();
        } else {
            response = Response.status(409).entity(bike).build();
        }
        return response;
    }


    // Return the list of todos to the user in the browser
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Bike> getBikes(@QueryParam("colour") String colour, @QueryParam("gender") String gender) {
        List<Bike> returnBikes = new ArrayList<>();
        if (colour != null && gender != null) {
            for (int i = 0; i < bikes.size(); i++) {
                if (bikes.get(i).getColour() == colour && bikes.get(i).getGender() == gender) {
                    returnBikes.add(bikes.get(i));
                }
            }
        }
        return returnBikes;
    }

    // Return the list of todos for applications
    @GET
    @Path("/{bikeid}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<String> getBikeDetails(@PathParam("bikeid") String bikeId) {
        List<String> details = new ArrayList<String>();
        for (int i = 0; i < bikes.size(); i++) {
            if(bikes.get(i).getId() == bikeId) {
                details.add(bikes.get(i).getId());
                details.add(bikes.get(i).getOwnerName());
                details.add(bikes.get(i).getColour());
                details.add(bikes.get(i).getGender());
            }
        }
        return details;
    }

    @POST
    @Path("/{bikeid}/order")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void orderBike(@PathParam("bikeid") String bikeid) {
        if(orderedBikes.get(bikeid).equals(false)) {
            orderedBikes.put(bikeid, true);
        }
    }
}
