package nl.utwente.di.dao;

import nl.utwente.di.model.Bike;

import java.util.HashMap;
import java.util.Map;

public enum BikeDao {
    instance;

    private Map<String, Bike> contentProvider = new HashMap<String, Bike>();

    BikeDao() {}

    public Map<String, Bike> getModel() {
        return contentProvider;
    }
}
